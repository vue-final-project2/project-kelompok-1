import NavbarPage from "../components/Navbar.vue";
import Cart from "../view/Cart.vue";
import Data from "../view/DataAdmin.vue";
import Home from "../components/Home.vue";
import Login from "../components/Login.vue";
import Register from "../components/Register.vue";
import MakananInstant from "@/menu/MakananInstant.vue"; 
import MakananRingan from "@/menu/MakananRingan.vue"; 
import MinumanDingin from "@/menu/MinumanDingin.vue"; 
import MinumanInstant from "@/menu/MinumanInstant.vue"; 
import ObatObatan from "@/menu/ObatObatan.vue"; 
import AlatTulis from "@/menu/AlatTulis.vue"; 
import BukuBuku from "@/menu/BukuBuku.vue"; 
import PerlengkapanSekolah from "@/menu/PerlengkapanSekolah.vue"; 
import AlatMakan from "@/menu/AlatMakan.vue"; 
import AlatKebersihan from "@/menu/AlatKebersihan.vue";
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
const routes = [
    {
    path: "/",
    name:"HomePage",
    component: Home
    },
    {
    path: "/login",
    name: "LoginPage",
    component: Login
    },

    {
    path: "/Register",
    name: "RegisterPage",
    component: Register
    },
    {
    path: "/cart",
    name:"CartPage",
    component: Cart
    },
    {
    path: "/navbar",
    name:"NavbarPage",
    component: NavbarPage
    },
    {
    path: "/data",
    name:"DataPage",
    component: Data
    },
    { 
        path: "/menu", 
        name: "MakananInstant", 
        component: MakananInstant, 
      }, 
      { 
        path: "/makananringan", 
        name: "MakananRingan", 
        component: MakananRingan, 
      }, 
      { 
        path: "/minumandingin", 
        name: "MinumanDingin", 
        component: MinumanDingin, 
      }, 
      { 
        path: "/minumaninstant", 
        name: "MinumanInstant", 
        component: MinumanInstant, 
      }, 
      { 
        path: "/obat", 
        name: "ObatObatan", 
        component: ObatObatan, 
      }, 
      { 
        path: "/alattulis", 
        name: "AlatTulis", 
        component: AlatTulis, 
      }, 
      { 
        path: "/buku", 
        name: "BukuBuku", 
        component: BukuBuku, 
      }, 
      { 
        path: "/perlengkapansekolah", 
        name: "PerlengkapanSekolah", 
        component: PerlengkapanSekolah, 
      }, 
      { 
        path: "/alatmakan", 
        name: "AlatMakan", 
        component: AlatMakan, 
      }, 
      { 
        path: "/alatkebersihan", 
        name: "AlatKebersihan", 
        component: AlatKebersihan, 
      },
]


const router = new Router({
    routes,
    mode: "history",
});

export default router;
